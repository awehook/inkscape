# SPDX-License-Identifier: GPL-2.0-or-later
name: inkscape
version-script: packaging/snappy/version.sh
version: devel # Replaced at build time ^^^
summary: Vector Graphics Editor
license: GPL-3.0
description: >
 Inkscape is a Free and open source vector graphics editor. It offers a rich
 set of features and is widely used for both artistic and technical
 illustrations such as cartoons, clip art, logos, typography, diagramming and
 flowcharting.

 It uses vector graphics to allow for sharp printouts and renderings at
 unlimited resolution and is not bound to a fixed number of pixels like raster
 graphics. Inkscape uses the standardized SVG file format as its main format,
 which is supported by many other applications including web browsers.

 Inkscape supports many advanced SVG features (markers, clones, alpha blending,
 etc.) and great care is taken in designing a streamlined interface.
 It is very easy to edit nodes, perform complex path operations, trace
 bitmaps and much more.

 We also aim to maintain a thriving user and developer community by using
 open, community-oriented development.
confinement: strict
grade: stable
base: core18

plugs:
  inkscape-config:
    interface: personal-files
    write: [ $HOME/.config/inkscape ]
  user-fonts:
    interface: personal-files
    read: [ $HOME/.local/share/fonts ]

slots:
  inkscape-dbus:
    interface: dbus
    bus: session
    name: org.inkscape.Inkscape

parts:
  inkscape:
    plugin: cmake
    source: .
    configflags:
      - '-DWITH_DBUS=ON'
      - '-DWITH_JEMALLOC=ON'
    build-packages:
      - cmake
      - intltool
      - libart-2.0-dev
      - libaspell-dev
      - libboost-dev
      - libcdr-dev
      - libdbus-glib-1-dev
      - libdouble-conversion-dev
      - libgc-dev
      - libgdl-3-dev
      - libgsl-dev
      - libgtkspell3-3-dev
      - libjemalloc-dev
      - liblcms2-dev
      - libmagick++-dev
      - libpoppler-private-dev
      - libpotrace-dev
      - librevenge-dev
      - libsigc++-2.0-dev
      - libsoup2.4-dev
      - libtool
      - libvisio-dev
      - libwpg-dev
      - libxml-parser-perl
      - libxml2-dev
      - libxslt1-dev
      - zlib1g-dev
    stage-packages:
      - aspell
      - libaspell15
      - libatkmm-1.6-1v5
      - libcairomm-1.0-1v5
      - libcdr-0.1-1
      - libdouble-conversion1
      - libgc1c2
      - libgdk-pixbuf2.0-0
      - libgdl-3-5
      - libglibmm-2.4-1v5
      - libglib2.0-bin
      - libgnomevfs2-0
      - libgsl23
      - libgslcblas0
      - libgtkmm-3.0-1v5
      - libgtkspell3-3-0
      - liblcms2-2
      - libjemalloc1
      - libmagick++-6.q16-7
      - libpangomm-1.4-1v5
      - libnspr4
      - libnss3
      - libpoppler-glib8
      - libpoppler73
      - libpotrace0
      - librevenge-0.0-0
      - libsigc++-2.0-0v5
      - libvisio-0.1-1
      - libwmf-bin
      - libwpg-0.3-3
      - libxslt1.1
      - imagemagick
      - libimage-magick-perl
      - transfig
      - libsvg-perl
      - libxml-xql-perl
      - ruby
    prime:
      - -lib/inkscape/*.a
      - -usr/lib/*/libharfbuzz*
    override-build: |
      sed -i.bak -e 's|Icon=${INKSCAPE_ICONPATH}|Icon=${SNAP}/share/inkscape/branding/inkscape.svg|g' $SNAPCRAFT_PART_SRC/org.inkscape.Inkscape.desktop.template
      snapcraftctl build
  python-deps:
    plugin: python
    python-version: python2
    python-packages:
      - lxml
      - numpy
      - scour
    stage-packages:
      - pstoedit
  cleanup:
    after: [ inkscape, python-deps ]
    plugin: nil
    build-snaps: [ core18, gnome-3-34-1804 ]
    override-prime: |
      set -eux
      for snap in "core18" "gnome-3-34-1804"; do
        cd "/snap/$snap/current" && find . -type f,l -exec rm -f "$SNAPCRAFT_PRIME/{}" \;
      done

apps:
  inkscape:
    command: bin/inkscape
    plugs:
      - home
      - gsettings
      - unity7
      - cups-control
      - removable-media
      - inkscape-config
      - user-fonts
    slots:
      - inkscape-dbus
    desktop: share/applications/org.inkscape.Inkscape.desktop
    environment:
      AAREALHOME: ${SNAP_USER_DATA}/../../..
      INKSCAPE_PROFILE_DIR: ${AAREALHOME}/.config/inkscape
      INKSCAPE_LOCALEDIR: ${SNAP}/share/locale
      INKSCAPE_DATADIR: ${SNAP}/share
      XDG_DATA_DIRS: ${XDG_DATA_DIRS}:${AAREALHOME}/.local/share
    extensions: [ gnome-3-34 ]
  viewer:
    command: bin/inkview
    plugs:
      - home
      - gsettings
      - unity7
      - removable-media
      - inkscape-config
      - user-fonts
    environment:
      AAREALHOME: ${SNAP_USER_DATA}/../../..
      INKSCAPE_PROFILE_DIR: ${AAREALHOME}/.config/inkscape
      INKSCAPE_LOCALEDIR: ${SNAP}/share/locale
      INKSCAPE_DATADIR: ${SNAP}/share
      XDG_DATA_DIRS: ${XDG_DATA_DIRS}:${AAREALHOME}/.local/share
    extensions: [ gnome-3-34 ]
